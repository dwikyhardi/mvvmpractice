import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:pretty_dio_logger/pretty_dio_logger.dart';

class DioService {
  Future<Dio> getClient() async {
    Dio dio;
    BaseOptions options;
    String baseUrl = "http://www.omdbapi.com/";

    options = new BaseOptions(
      baseUrl: baseUrl,
      connectTimeout: Duration(seconds: 30).inMilliseconds,
      receiveTimeout: Duration(seconds: 30).inMilliseconds,
    );
    dio = new Dio(options);

    dio.interceptors.add(InterceptorsWrapper(
      onRequest: (RequestOptions option) async {
        print("============= onRequest =============");
      },
      onResponse: (Response response) async {
        print("============= onResponse =============");
        print(jsonEncode(response.data));
      },
      onError: (DioError e) async {
        print("============= onError =============");

      },
    ));

    dio.interceptors.add(PrettyDioLogger(
        error: true,
        request: true,
        requestBody: true,
        requestHeader: true,
        responseBody: true,
        responseHeader: true,
        compact: true,
        maxWidth: 200));

    return dio;
  }
}
