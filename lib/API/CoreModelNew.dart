import 'package:json_annotation/json_annotation.dart';

import 'Meta.dart';

part 'CoreModelNew.g.dart';

@JsonSerializable()

class CoreModelNew {

  Meta meta;
  dynamic data;

  CoreModelNew(this.meta, this.data);

  factory CoreModelNew.fromJson(Map<String, dynamic> json) => _$CoreModelNewFromJson(json);


  Map<String, dynamic> toJson() => _$CoreModelNewToJson(this);


}
