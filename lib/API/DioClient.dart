import 'package:dio/dio.dart';
import 'package:mvvmpractice/API/CoreModelNew.dart';
import 'package:retrofit/http.dart';

part 'DioClient.g.dart';

@RestApi(baseUrl: '')
abstract class DioClient {
  factory DioClient(Dio dio, {String baseUrl}) = _DioClient;

  @GET("")
  Future<Map<String, dynamic>> getMovieSearch(@Query('s') String search, @Query('apikey') String apiKey);

  @GET("")
  Future<Map<String, dynamic>> getMovieDetails(@Query('i') String imdbID,@Query('plot') String plot, @Query('apikey') String apiKey);

}
