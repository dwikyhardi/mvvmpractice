// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'CoreModelNew.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CoreModelNew _$CoreModelNewFromJson(Map<String, dynamic> json) {
  return CoreModelNew(
    json['meta'] == null
        ? null
        : Meta.fromJson(json['meta'] as Map<String, dynamic>),
    json['data'],
  );
}

Map<String, dynamic> _$CoreModelNewToJson(CoreModelNew instance) =>
    <String, dynamic>{
      'meta': instance.meta,
      'data': instance.data,
    };
