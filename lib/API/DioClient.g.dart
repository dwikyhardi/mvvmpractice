// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'DioClient.dart';

// **************************************************************************
// RetrofitGenerator
// **************************************************************************

class _DioClient implements DioClient {
  _DioClient(this._dio, {this.baseUrl}) {
    ArgumentError.checkNotNull(_dio, '_dio');
  }

  final Dio _dio;

  String baseUrl;

  @override
  getMovieSearch(search, apiKey) async{
    ArgumentError.checkNotNull(apiKey, 'apikey');
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{'s': search, 'apikey' :apiKey};
    queryParameters.removeWhere((key, value) => value == null);
    final _data = <String, dynamic>{};
    final Response<Map<String, dynamic>> _result = await _dio.request(
        '',
        queryParameters: queryParameters,
        options: RequestOptions(
            method: 'GET',
            headers: <String, dynamic>{},
            extra: _extra,
            baseUrl: baseUrl),
        data: _data);
    return Future.value(_result.data);
  }

  @override
  getMovieDetails(imdbId, plot, apiKey) async{
    ArgumentError.checkNotNull(apiKey, 'apikey');
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{'i': imdbId, 'plot': plot, 'apikey' :apiKey};
    queryParameters.removeWhere((key, value) => value == null);
    final _data = <String, dynamic>{};
    final Response<Map<String, dynamic>> _result = await _dio.request(
        '',
        queryParameters: queryParameters,
        options: RequestOptions(
            method: 'GET',
            headers: <String, dynamic>{},
            extra: _extra,
            baseUrl: baseUrl),
        data: _data);
    return Future.value(_result.data);
  }



}
