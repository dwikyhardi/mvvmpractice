import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mvvmpractice/MODEL/MovieDetailModel.dart';

class MovieDetails extends StatelessWidget {

  final MovieDetailsModel movieDetailsModel;

  MovieDetails(this.movieDetailsModel);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Image.network(movieDetailsModel.poster),
            Text(movieDetailsModel.title),
            Text(movieDetailsModel.genre),
            Text(movieDetailsModel.year),
            Text(movieDetailsModel.type),
            Text(movieDetailsModel.plot,textAlign: TextAlign.center,),
            Text(movieDetailsModel.actors),
            Text(movieDetailsModel.awards),
            Text(movieDetailsModel.country),
            Text(movieDetailsModel.director),
          ],
        ),
      ),
    );
  }
}
