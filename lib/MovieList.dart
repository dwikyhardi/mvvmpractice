import 'package:flutter/material.dart';
import 'package:mvvmpractice/MovieDetails.dart';
import 'package:mvvmpractice/VIEWMODEL/MovieDataViewModel.dart';

class MovieList extends StatelessWidget {
  final List<MovieDataViewModel> movies;
  final MovieDataListViewModel movieDataListViewModel;

  MovieList({this.movies, this.movieDataListViewModel});

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemCount: this.movies.length,
      itemBuilder: (context, index) {
        final movie = this.movies[index];
        return ListTile(
          contentPadding: EdgeInsets.all(10),
          onTap: () {
            showDetailMovie(movie.imdbID, context);
          },
          leading: Container(
            decoration: BoxDecoration(
                image: DecorationImage(
                  fit: BoxFit.cover,
                  image: NetworkImage(movie.poster),
                ),
                borderRadius: BorderRadius.circular(6)),
            width: 50,
            height: 100,
          ),
          title: Text(movie.title),
        );
      },
    );
  }

  showDetailMovie(String imdbID, BuildContext context) {
    movieDataListViewModel.getDetailsMovie(imdbID).whenComplete(() {
      showBottomSheet(
          context: context,
          builder: (BuildContext buildContext) =>
              MovieDetails(movieDataListViewModel.details),);
//      showModalBottomSheet(
//          context: context,
//          builder: (BuildContext buildContext)
//            => MovieDetails(movieDataListViewModel.details));
    });
  }
}
