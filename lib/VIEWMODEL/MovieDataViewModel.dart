import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:mvvmpractice/API/DioClient.dart';
import 'package:mvvmpractice/API/DioService.dart';
import 'package:mvvmpractice/MODEL/MovieData.dart';
import 'package:mvvmpractice/MODEL/MovieDetailModel.dart';

class MovieDataListViewModel extends ChangeNotifier{

  List<MovieDataViewModel> movieDataViewModel = List<MovieDataViewModel>();
  var details = MovieDetailsModel();
  var apiKey = "cb52248d";

  Future<void> fetchMovie(String keyword) async {
    var client = DioClient(await DioService().getClient());
    var getMovieSearch = await client.getMovieSearch(keyword, apiKey);
    this.movieDataViewModel.clear();
    if(getMovieSearch['Response'] == "True"){
      print(jsonEncode(getMovieSearch['Search']));
      var searchData = jsonDecode(jsonEncode(getMovieSearch['Search']));
      for(int i = 0; i < searchData.length; i++){
        var data = MovieData.fromJson(searchData[i]);
        this.movieDataViewModel.add(MovieDataViewModel(movieData: data));
      }
    }
    print(movieDataViewModel);
    notifyListeners();
  }

  Future<void> getDetailsMovie(String imdbId) async {
    var client = DioClient(await DioService().getClient());
    var getMovieSearch = await client.getMovieDetails(imdbId, "full", apiKey);
    if(getMovieSearch['Response'] == "True"){
      details = MovieDetailsModel.fromJson(jsonDecode(jsonEncode(getMovieSearch)));
    }
    notifyListeners();
  }

}

class MovieDataViewModel {
  final MovieData movieData;

  MovieDataViewModel({this.movieData});

  String get title{
    return this.movieData.title;
  }

  String get poster{
    return this.movieData.poster;
  }

  String get imdbID{
    return this.movieData.imdbID;
  }

  String get type{
    return this.movieData.type;
  }

  String get year{
    return this.movieData.year;
  }

}
