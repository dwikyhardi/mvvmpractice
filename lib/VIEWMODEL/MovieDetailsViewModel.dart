//import 'dart:convert';
//
//import 'package:flutter/cupertino.dart';
//import 'package:mvvmpractice/API/DioClient.dart';
//import 'package:mvvmpractice/API/DioService.dart';
//import 'package:mvvmpractice/MODEL/MovieDetailModel.dart';
//
//class MovieDetailsViewModel extends ChangeNotifier{
//
//  var details = MovieDetailsModel();
//  var apiKey = "cb52248d";
//
//  Future<void> getDetailsMovie(String imdbId) async {
//    var client = DioClient(await DioService().getClient());
//    var getMovieSearch = await client.getMovieDetails(imdbId, "full", apiKey);
//    if(getMovieSearch['Response'] == "True"){
//      details = MovieDetailsModel.fromJson(jsonDecode(jsonEncode(getMovieSearch)));
//    }
//    notifyListeners();
//  }
//
//}