class MovieData {
  final String title;
  final String poster;
  final String year;
  final String type;
  final String imdbID;

  MovieData({this.title, this.poster, this.year, this.type, this.imdbID});

  factory MovieData.fromJson(Map<String, dynamic> json) {
    return MovieData(
        title: json["Title"],
        poster: json["Poster"],
        year: json["Year"],
        type: json["Tyoe"],
        imdbID: json["imdbID"]);
  }
}
